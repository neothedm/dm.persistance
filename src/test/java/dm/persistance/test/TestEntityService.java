package dm.persistance.test;

import dm.services.GenericService;
import dm.services.ServiceProvider;

import java.util.List;

/**
 * Created by neo on 1/3/14.
 */
public class TestEntityService implements GenericService<TestEntity> {

    TestEntityDao testEntityDao = new TestEntityDao();

    @Override
    public void register() {
        ServiceProvider.register(TestEntity.class, this);
    }

    @Override
    public TestEntity createEntity() {
        return new TestEntity();
    }

    @Override
    public void delete(TestEntity t) {
        testEntityDao.remove(t);
    }

    @Override
    public void save(TestEntity t) {
        testEntityDao.save(t);
    }

    @Override
    public void update(TestEntity t) {
        testEntityDao.update(t);
    }

    @Override
    public void refresh(TestEntity t) {
        testEntityDao.refresh(t);
    }

    @Override
    public List<TestEntity> getAll() {
        return testEntityDao.getAll();
    }

    @Override
    public TestEntity findById(long id) {
        return testEntityDao.get(id);
    }

    public TestEntity findByName(String name) {
        return testEntityDao.getByUnique("name", name);
    }
}
