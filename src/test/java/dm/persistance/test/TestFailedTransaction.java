package dm.persistance.test;

import dm.persistance.HibernateFactory;
import dm.persistance.Transaction;
import junit.framework.TestCase;

import java.util.List;

/**
 * Created by neo on 1/3/14.
 */
public class TestFailedTransaction extends TestCase {
    @Override
    protected void setUp() throws Exception {
        HibernateFactory.initialize();
    }

    public void test() {
        transactionFailure();
        entityDaoSave();
        entityDaoFind();
        entityDaoDelete();
    }

    public void transactionFailure() {
        try {
        Transaction.execute(session -> {
            TestEntity testEntity = new TestEntity();
            testEntity.setDescription("description");
            testEntity.setName("name");
            session.save(testEntity);
            Transaction.execute(session2 -> {
                TestEntity testEntity2 = new TestEntity();
                testEntity2.setName("name2");
                testEntity2.setDescription("description2");
                session2.save(testEntity2);
                throw new Exception();
            });
            return null;
        });
        } catch (Exception e) {
            // Ignore
        }
        List testEntities = Transaction.execute(session -> session.createQuery("from dm.persistance.test.TestEntity as testEntity").list());
        assertEquals(testEntities.size(), 0);
    }

    public void entityDaoSave() {
        TestEntity testEntity = getDummyTestEntity();
        TestEntityService service = getService();
        service.save(testEntity);
        assertNotNull(service.findByName("test name"));
    }

    public void entityDaoFind() {
        TestEntityService service = getService();
        TestEntity testEntity = service.findByName("test name");
        assertNotNull(testEntity);
    }

    public void entityDaoDelete() {
        TestEntityService service = getService();
        service.delete(service.findByName("test name"));
        assertNull(service.findByName("test name"));
    }

    public TestEntityService getService() {
        return new TestEntityService();
    }

    public TestEntity getDummyTestEntity() {
        TestEntity testEntity = new TestEntity();
        testEntity.setName("test name");
        testEntity.setDescription("test description");
        return testEntity;
    }

    @Override
    protected void tearDown() throws Exception {
        HibernateFactory.shutdown();
    }
}
