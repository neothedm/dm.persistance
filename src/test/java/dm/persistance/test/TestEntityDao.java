package dm.persistance.test;

import dm.persistance.GenericDaoHibernate;

/**
 * Created by neo on 1/4/14.
 */
public class TestEntityDao extends GenericDaoHibernate<TestEntity, Long> {
    public TestEntityDao() {
        super(TestEntity.class);
    }
}
