package dm.persistance;

import org.hibernate.Session;

/**
 * Transaction container
 */
public class Transaction {
    public static <Result> Result execute(IHandler<Result> handler) {
        boolean newSession = false;
        boolean newTransaction = false;
        Session session = HibernateFactory.getSessionFactory().getCurrentSession();
        if (session == null) {
            session = HibernateFactory.getSessionFactory().openSession();
            newSession = true;
        }
        org.hibernate.Transaction transaction = session.getTransaction();
        if (!transaction.isActive()) {
            transaction = session.beginTransaction();
            newTransaction = true;
        }
        Result result;
        try {
            result = handler.execute(session);
            if (newTransaction) {
                transaction.commit();
            }
        } catch (Exception e) {
            if (transaction != null)
                transaction.rollback();
            throw new IllegalStateException(e);
        } finally {
            if (newSession)
                session.close();
        }
        return result;
    }
}
