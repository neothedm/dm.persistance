package dm.persistance;

import dm.services.BasicDbEntityService;
import dm.services.ServiceProvider;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class DbEntity<ENTITY> extends BasicDbEntityService {

    public abstract long getId();

    public void save() throws Exception {
        save(this);
    }

    public void update() throws Exception {
        update(this);
    }

    public void delete() throws Exception {
        delete(this);
    }

    public void refresh() throws Exception {
        refresh(this);
    }
}
