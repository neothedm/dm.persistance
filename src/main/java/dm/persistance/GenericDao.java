package dm.persistance;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public interface GenericDao<T, PK extends Serializable> {

    List<T> getAll();

    T get(final PK id);

    boolean exists(PK id);

    T save(final T object);

    void remove(T object);

    void replicate(T object);

    void refresh(T object);

    List<T> getBy(String column, Object value);

    List<T> getBy(String column, Object value, int limit);

    List<T> getLike(String column, Object value);

    T update(T object);

    T getByUnique(String column, Object value);

    List<T> getBeforeDate(String column, Date date);

    List<T> getAfterDate(String column, Date date);
}
