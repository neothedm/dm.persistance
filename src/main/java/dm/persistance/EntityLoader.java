package dm.persistance;

import dm.services.GenericService;
import dm.services.ServiceProvider;

/**
 * Fast way to handle entities
 */
public class EntityLoader {

    public static <E> void save(E obj) {
        GenericService<E> service = ServiceProvider.get(obj);
        service.save(obj);
    }

    public static <E> void update(E obj) {
        GenericService<E> service = ServiceProvider.get(obj);
        service.update(obj);
    }

    public static <E> void delete(E obj) {
        GenericService<E> service = ServiceProvider.get(obj);
        service.delete(obj);
    }

    public static <E> E find(Class<E> clazz, Long id) {
        GenericService<E> genericService = ServiceProvider.get(clazz);
        return genericService.findById(id);
    }
}
