package dm.persistance;

import org.hibernate.Session;

/**
 * Implemented as an anonymous class to contain a transaction
 */
@FunctionalInterface
public interface IHandler<T> {
    public T execute(Session session) throws Exception;
}

