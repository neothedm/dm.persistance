package dm.persistance;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateFactory {
    private static SessionFactory sessionFactory;

    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null)
            initialize();
        return sessionFactory;
    }

    public static Session getCurrentSession() {
        return getSessionFactory().getCurrentSession();
    }

    public static void initialize() {
        sessionFactory = new Configuration().configure().buildSessionFactory();
    }

    public static void closeSession() {
        Session sess = HibernateFactory.getSessionFactory()
                .getCurrentSession();
        if (sess.getTransaction().isActive()) {
            sess.getTransaction().commit();
        }
        if (sess.isOpen()) {
            sess.close();
        }
    }

    public static void shutdown() {
        sessionFactory.close();
    }
}
