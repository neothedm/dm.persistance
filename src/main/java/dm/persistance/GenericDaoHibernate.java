package dm.persistance;

import org.hibernate.ReplicationMode;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: neo
 * Date: 8/23/13
 * Time: 3:44 AM
 * To change this template use File | Settings | File Templates.
 */
public abstract class GenericDaoHibernate<T, PK extends Serializable> implements GenericDao<T, PK> {


    protected Class<T> persistentClass;

    public GenericDaoHibernate(Class<T> persistentClass) {
        this.persistentClass = persistentClass;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<T> getAll() {
        return Transaction.execute(session -> session.createQuery("from " + persistentClass.getName()).list());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T get(final PK id) {
        return Transaction.execute(session -> (T) session.get(persistentClass, id));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean exists(PK id) {
        T entity = get(id);
        return entity != null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T save(final T object) {
        return Transaction.execute(session -> (T) session.save(object));

    }

    @Override
    public T update(final T object) {
        return Transaction.execute(session -> {
            session.update(object);
            return object;
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void replicate(final T object) {
        Transaction.execute(session -> {
            session.replicate(object, ReplicationMode.EXCEPTION);
            return null;
        });
    }

    @Override
    public void refresh(final T object) {
        Transaction.execute(session -> {
            session.refresh(object);
            return object;
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove(final T t) {
        Transaction.execute(session -> {
            session.delete(t);
            return null;
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<T> getBy(final String column, final Object value) {
        return getBy(column, value, -1);
    }

    @Override
    public List<T> getBy(String column, Object value, int limit) {
        return Transaction.execute(session -> session.createQuery("from " + persistentClass.getName() + " as entity where entity." + column + " = :value").setParameter("value", value).setMaxResults(limit).list());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T getByUnique(final String column, final Object value) {
        return Transaction.execute(session -> (T) session.createQuery("from " + persistentClass.getName() + " as entity where entity." + column + " = :value").setParameter("value", value).uniqueResult());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<T> getLike(final String column, final Object value) {
        return Transaction.execute(session -> session.createQuery("from " + persistentClass.getName() + " as entity where entity." + column + " like :value").setParameter("value", value).list());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<T> getBeforeDate(final String column, final Date date) {
        return Transaction.execute(session -> session.createQuery("from " + persistentClass.getName() + " as entity where entity." + column + " < :value").setDate("value", date).list());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<T> getAfterDate(final String column, final Date date) {
        return Transaction.execute(session -> session.createQuery("from " + persistentClass.getName() + " as entity where entity." + column + " > :value").setDate("value", date).list());
    }
}
