package dm.persistance;

public class BasicDbDao extends GenericDaoHibernate<DbEntity, Long> {
    public BasicDbDao() {
        super(DbEntity.class);
    }
}
