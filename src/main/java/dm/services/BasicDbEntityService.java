package dm.services;

import dm.persistance.BasicDbDao;
import dm.persistance.DbEntity;

import java.util.List;

public class BasicDbEntityService implements GenericService<DbEntity> {

    BasicDbDao entityDao = new BasicDbDao();

    public BasicDbEntityService() {
        register();
    }

    @Override
    public void register() {
        ServiceProvider.register(DbEntity.class, this);
    }

    @Override
    public DbEntity createEntity() {
        throw new IllegalStateException("Not Supported!");
    }

    @Override
    public void delete(DbEntity t) {
        entityDao.remove(t);
    }

    @Override
    public void save(DbEntity t) {
        entityDao.save(t);
    }

    @Override
    public void update(DbEntity t) {
        entityDao.update(t);
    }

    @Override
    public void refresh(DbEntity t) {
        entityDao.refresh(t);
    }

    @Override
    public List<DbEntity> getAll() {
        return entityDao.getAll();
    }

    @Override
    public DbEntity findById(long id) {
        return entityDao.get(id);
    }
}
