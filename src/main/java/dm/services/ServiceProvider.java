package dm.services;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Service central repository, all services should register them selves
 */
public class ServiceProvider {

    private final static Object lock = new Object();

    private static Map<Class, GenericService> services = Collections.synchronizedMap(new HashMap<>());
    private static Map<String, GenericService> serviceImplementationMap = Collections.synchronizedMap(new HashMap<>());

    /**
     * Register services for given entities
     * @return true if non existence service exists, false if it does
     */
    public static <E, S extends GenericService<E>> boolean register(Class<E> clazz, S service) {
        synchronized (lock) {
            @SuppressWarnings("unchecked")
            S existingService = (S) services.get(clazz);
            if (existingService != null) {
                return false;
            } else {
                services.put(clazz, service);
                serviceImplementationMap.put(service.getClass().getName(), service);
                return true;
            }
        }
    }

    /**
     * @return registered service for the given class
     */
    public static <E, S extends GenericService<E>> S get(Class<E> clazz) {
        synchronized (lock) {
            @SuppressWarnings("unchecked")
            S service = (S) services.get(clazz);
            if (service == null) {
                throw new IllegalArgumentException("No service found for class" + clazz.getSimpleName());
            }
            return service;
        }
    }

    /**
     * @return registered service for the given entity
     */
    public static <E, S extends GenericService<E>> S get(E entity) {
        @SuppressWarnings("unchecked")
        S service = (S) get(entity.getClass());
        return service;
    }

    public static <S extends GenericService> S getServiceInstance(String serviceImplementation) {
        @SuppressWarnings("unchecked")
        S genericService =  (S) serviceImplementationMap.get(serviceImplementation);
        return genericService;
    }
}
