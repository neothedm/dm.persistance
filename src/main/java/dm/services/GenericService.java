package dm.services;

import java.util.List;

/**
 * The sole purpose of this services is to handle entities storage and manipulation
 */
public interface GenericService<ENTITY> {

    /**
     * Register this service to the ServiceProvider
     */
    void register();

    /**
     * Create new entity which this service serves
     */
    ENTITY createEntity();

    /**
     * Remove passed entity
     * @param t entity to remove
     */
    void delete(ENTITY t);

    /**
     * Saves passed entity
     * @param t entity to save
     */
    void save(ENTITY t);

    /**
     * Updates passed entity
     * @param t entity to update
     */
    void update(ENTITY t);

    /**
     * Refresh passed entity
     * @param t entity to refresh
     */
    void refresh(ENTITY t);

    /**
     * @return all stored entities
     */
    List<ENTITY> getAll();

    /**
     * Finds the entity by Id
     * @param id entity id
     * @return matching entity
     */
    ENTITY findById(long id);
}

